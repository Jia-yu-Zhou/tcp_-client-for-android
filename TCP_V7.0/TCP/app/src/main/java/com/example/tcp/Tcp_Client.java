package com.example.tcp;

import android.os.Message;
import android.util.Log;
import android.view.animation.Animation;
import android.webkit.WebSettings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.charset.StandardCharsets;

public class Tcp_Client {
    public boolean isConnect = false;//客户端连接状态
    private String Host;//IP地址
    private int Port;//端口
    private Socket socket;//套接字
    private InputStream inputStream;//输入流
    private OutputStream outputStream;//输出流
    private MyHandler myHandler;//信息处理
    private MyWebView myWebView;

    public Tcp_Client(String host, int port, MyHandler Handler) {
        Host = host;
        Port = port;
        myHandler = Handler;
    }

    public void Connect(){
        /* 创建套接字 */
        try {
            socket = new Socket();
            SocketAddress socketAddress = new InetSocketAddress(Host, Port);
            socket.connect(socketAddress,500);
            Log.v("Tcp_Client_Thread","正在连接服务器... ...");
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            Log.v("Tcp_Client_Thread","服务器连接成功");
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Send(MainActivity.Control_mode_standing+"\r\n");//连接上立马设为站立模式
                    Send(MainActivity.Control_light_off+"\r\n");//连接上立马关灯
                }
            }).start();

            Message message = new Message();
            message.what = myHandler.Connect_successful;
            myHandler.trans_port(Host);//把ip通过myHandler传给stream推流
            myHandler.sendMessage(message);
            isConnect = true;
            /* 开启接收 */
            Receive();
        } catch (IOException e) {
            Log.v("Tcp_Client_Thread","服务器连接失败");
            Message message = new Message();
            message.what = myHandler.Connect_failed;
            myHandler.sendMessage(message);
            Close_All();
            e.printStackTrace();
        }
    }

    private void Receive() {
        if(isConnect) {
            try {
                Reader reader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(reader);
                String buf = null;
                while (true) {
                    if ((buf = bufferedReader.readLine()) != null) {
                        Log.v("Tcp_Client_Thread:", "Receive from Server:" + buf);
                        Message message = new Message();
                        message.what = myHandler.Receive_successful;
                        message.obj = buf;
                        myHandler.sendMessage(message);
                    } else {
                        Log.v("Tcp_Client_Thread:", "连接失败");
                        Message message = new Message();
                        message.what = myHandler.Connect_failed;
                        myHandler.sendMessage(message);
                        Close_All();
                        break;
                    }
                }
            } catch (IOException e) {//服务器异常关闭
                Log.v("Tcp_Client_Thread:", "连接失败");
                Message message = new Message();
                message.what = myHandler.Connect_failed;
                myHandler.sendMessage(message);
                Close_All();
                e.printStackTrace();
            }
        }
    }

    public void Send(String buf){
        if(isConnect){
            try {
                outputStream.write(buf.getBytes(StandardCharsets.UTF_8));
                outputStream.flush();
                Log.v("Tcp_Client_Thread","向服务器发送："+buf);
            } catch (IOException e) {
                Log.v("Tcp_Client_Thread","发送失败");
                Close_All();
                e.printStackTrace();
            }
        }
    }

    public void Close_All(){
        try {
            isConnect = false;
            if(socket != null)socket.close();
            if(outputStream != null)outputStream.close();
            if(inputStream != null)inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
