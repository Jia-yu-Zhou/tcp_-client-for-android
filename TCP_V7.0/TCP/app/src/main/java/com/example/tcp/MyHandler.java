package com.example.tcp;

import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.ViewStructure;
import android.view.animation.Animation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.security.Principal;
import java.security.PublicKey;

public class MyHandler extends Handler {

    public final static int Connect_successful = 2;
    public final static int Connect_failed = 3;
    public final static int Receive_successful = 4;
    public final static int Button_mode = 5;
    public final static int Button_motor = 6;
    public final static int Button_Steering = 7;
    public final static int Button_light = 8;

    private String host;
    private TextView textView_State;
    private Button button_Mode_standing;
    private Button button_Mode_telecontrol;
    private Button button_Mode_obstacle;
    private Button button_Disconnect;
    private Button button_light;
    private MyWebView myWebView;

    public MyHandler(@NonNull Looper looper,
                     TextView TextView_State,
                     Button Button_Mode_standing,
                     Button Button_Mode_telecontrol,
                     Button Button_Mode_obstacle,
                     Button Button_Disconnect,
                     Button Button_light,
                     MyWebView MWebView) {
        super(looper);
        textView_State = TextView_State;
        button_Mode_standing = Button_Mode_standing;
        button_Mode_telecontrol = Button_Mode_telecontrol;
        button_Mode_obstacle = Button_Mode_obstacle;
        button_Disconnect = Button_Disconnect;
        button_light = Button_light;
        myWebView = MWebView;
    }

    public void trans_port(String Host)
    {
        host = Host;
    }

    @Override
    public void handleMessage(@NonNull Message msg) {
        super.handleMessage(msg);
        switch (msg.what){
            case Connect_successful://连接成功
                textView_State.setText("连接成功");
                button_Disconnect.setText("断开");
                /* mjpeg-stream 推流 */
                myWebView.setVisibility(myWebView.VISIBLE);//显示WebView窗口
                WebSettings webSettings = myWebView.getSettings();
                webSettings.setJavaScriptEnabled(true);
                /* 水平镜像 */
                Animation animation = new Rotate3dAnimation(myWebView.getWidth() / 2, myWebView.getHeight() / 2);
                animation.setFillAfter(true);
                myWebView.startAnimation(animation);
                /* 连接mjpeg-stream */
                myWebView.loadUrl("http://"+host+":8080/?action=stream");
                myWebView.setInitialScale(420);//缩放比例
                myWebView.scrollTo(380,325);//重写onScrollChanged方法监听滚动，一直默认这个位置
                /* 不显示滚动条 */
                myWebView.setScrollContainer(false);
                myWebView.setVerticalScrollBarEnabled(false);
                myWebView.setHorizontalScrollBarEnabled(false);

                /* 连接成功立马设为站立模式 */
                textView_State.setText("站立模式");
                button_Mode_standing.setTextColor(Color.rgb(0,255,0));
                button_Mode_telecontrol.setTextColor(Color.rgb(255,255,255));
                button_Mode_obstacle.setTextColor(Color.rgb(255,255,255));

                /* 连接成功默认关灯 */
                button_light.setText("开灯");
                button_light.setTextColor(Color.rgb(0,255,0));

                break;
            case Button_light:
                if((boolean)msg.obj){
                    button_light.setText("关灯");
                    button_light.setTextColor(Color.rgb(255,0,0));
                }
                else{
                    button_light.setText("开灯");
                    button_light.setTextColor(Color.rgb(0,255,0));
                }
                break;

            case Connect_failed://连接失败
                textView_State.setText("连接失败");
                button_Disconnect.setText("未连接");
                break;

            case Button_mode://更改小车模式事件
                Text_State_Switch_Mode_To_Display((int)msg.obj);
                break;

            case Button_motor://小车遥控动作事件
                Text_State_Switch_Motor_To_Display((String)msg.obj);
                break;

            case Button_Steering://舵机遥控动作事件
                Text_State_Switch_Steering_To_Display((String)msg.obj);
                break;

            case Receive_successful:
                System.out.println((String)msg.obj);
                //textView_State.setText(msg.obj.toString());
                break;

            default:break;
        }
    }

    /* 按下模式选择按钮的显示选择    在m_Handler调用 */
    public void Text_State_Switch_Mode_To_Display(int mode){
        switch (mode) {
            case 1:
                textView_State.setText("站立模式");
                button_Mode_standing.setTextColor(Color.rgb(0,255,0));
                button_Mode_telecontrol.setTextColor(Color.rgb(255,255,255));
                button_Mode_obstacle.setTextColor(Color.rgb(255,255,255));
                break;

            case 2:
                textView_State.setText("遥控模式");
                button_Mode_standing.setTextColor(Color.rgb(255,255,255));
                button_Mode_telecontrol.setTextColor(Color.rgb(0,255,0));
                button_Mode_obstacle.setTextColor(Color.rgb(255,255,255));
                break;

            case 3:
                textView_State.setText("避障模式");
                button_Mode_standing.setTextColor(Color.rgb(255,255,255));
                button_Mode_telecontrol.setTextColor(Color.rgb(255,255,255));
                button_Mode_obstacle.setTextColor(Color.rgb(0,255,0));
                break;

            default:break;
        }
    }

    /* 按下遥控电机控制按钮的显示选择 在m_Handler调用 */
    public void Text_State_Switch_Motor_To_Display(String motor){
        switch (motor) {
            case "f":
                textView_State.setText("前进");
                break;

            case "b":
                textView_State.setText("后退");
                break;

            case "l":
                textView_State.setText("左转");
                break;

            case "r":
                textView_State.setText("右转");
                break;

            case "0":
                textView_State.setText("停止");
                break;

            default:break;
        }
    }

    /* 按下遥控舵机控制按钮的显示选择 在m_Handler调用 */
    public void Text_State_Switch_Steering_To_Display(String Steering){
        switch (Steering) {
            case "u":
                textView_State.setText("上抬");
                break;

            case "d":
                textView_State.setText("下压");
                break;

            case "l":
                textView_State.setText("左转");
                break;

            case "r":
                textView_State.setText("右转");
                break;

            case "R":
                textView_State.setText("复位");
                break;

            default:break;
        }
    }


}
