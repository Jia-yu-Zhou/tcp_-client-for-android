package com.example.tcp;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import java.sql.Struct;

public class MoveButton extends View {

    public String ID;//区分左右摇杆
    public float Max_Distance = 130;//外圆半径
    public double lastX;
    public double lastY;
    public double Draw_x;//通过距离换算，让内圆始终在外圆内的绘图距离
    public double Draw_y;


    public MoveButton(Context context) {
        super(context);
    }

    public MoveButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MoveButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MoveButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void MoveButton_left_Init(String id){
        ID = id;
    }

    public void Trans_Draw(double Real_X, double Real_Y){
        double Real_X_abs = Math.abs(Real_X);
        double Real_Y_abs = Math.abs(Real_Y);
        double Draw_x_abs;
        double Draw_y_abs;
        double Distance = Math.sqrt(Math.pow(Real_X_abs,2)+Math.pow(Real_Y_abs,2));
        if(Distance > Max_Distance){
            if (Real_X_abs != 0){
                double angle = Math.atan(Real_Y_abs/Real_X_abs);//弧度值
                Draw_x_abs = (Max_Distance*Math.cos(angle));
                Draw_y_abs = (Max_Distance*Math.sin(angle));
            }
            else{
                Draw_x_abs = 0;
                Draw_y_abs = Max_Distance;
            }
        }
        else {
            Draw_x_abs = Real_X_abs;
            Draw_y_abs = Real_Y_abs;
        }
        if(Real_X>0) Draw_x = Draw_x_abs;
        else Draw_x = -Draw_x_abs;
        if(Real_Y >0) Draw_y = Draw_y_abs;
        else Draw_y = -Draw_y_abs;
    }

    public String Move_Button_motor_value(double draw_x, double draw_y){
        String value = new String();
        if(draw_x-draw_y > 0 && draw_x+draw_y < 0){
            value = MainActivity.Control_motor_forward;
        }
        else if(draw_x-draw_y < 0 && draw_x+draw_y > 0){
            value = MainActivity.Control_motor_back;
        }
        else if(draw_x-draw_y < 0 && draw_x+draw_y < 0){
            value = MainActivity.Control_motor_left;
        }
        else if(draw_x-draw_y > 0 && draw_x+draw_y > 0){
            value = MainActivity.Control_motor_right;
        }
        else if(draw_x == 0 && draw_y == 0){
            value = MainActivity.Control_stop;
        }
        return value;
    }

    public String Move_Button_steering_value(double draw_x, double draw_y){
        String value = new String();
        if(draw_x-draw_y > 0 && draw_x+draw_y < 0){
            value = MainActivity.Control_steering_up;
        }
        else if(draw_x-draw_y < 0 && draw_x+draw_y > 0){
            value = MainActivity.Control_steering_down;
        }
        else if(draw_x-draw_y < 0 && draw_x+draw_y < 0){
            value = MainActivity.Control_steering_left;
        }
        else if(draw_x-draw_y > 0 && draw_x+draw_y > 0){
            value = MainActivity.Control_steering_right;
        }
        else if(draw_x == 0 && draw_y == 0){
            value = MainActivity.Control_stop;
        }
        return value;
    }
}


